const mongoose = require('mongoose');
const config = require('./config');

const User = require('./models/User');
const Item = require('./models/Item');
const Category = require('./models/Category');

mongoose.connect(config.db.url + '/' + config.db.name);

const db = mongoose.connection;

db.once('open', async () => {
    try {
        await db.dropCollection('categories');
        await db.dropCollection('users');
        await db.dropCollection('items');

    } catch (e) {
        console.log('Collections were not present, skipping drop...');
    }

    const [Furniture, Notebooks, Food, HomeAppliances] = await Category.create({
            name: 'Furniture'
        }, {
            name: 'Notebooks'
        }, {
            name: 'Food'
        }, {
            name: 'Home appliances'
        }
    );

    const [first, second] = await User.create({
        username: 'aijan',
        displayName: 'Aijan',
        password: '1234',
        phoneNumber: '+996779618282'
    }, {
        username: 'emir',
        displayName: 'Emir',
        password: '1234',
        phoneNumber: '+996771943742'
    });

    await Item.create({
        title: 'Banana',
        description: 'Yellow ones)',
        category: Food._id,
        image: 'banana.jpeg',
        user: first._id,
        price: 30
    }, {
        title: 'Macbook pro',
        description: '2016',
        category: Notebooks._id,
        image: 'mac.jpg',
        user: second._id,
        price: 600000
    }, {
        title: 'Washing machine',
        description: 'LG',
        category: HomeAppliances._id,
        image: 'washing.jpg',
        user: second._id,
        price: 45000
    });

    console.log('Fixtures done!')
    db.close();
});