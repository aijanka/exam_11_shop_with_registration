const mongoose = require('mongoose');
const nanoid = require('nanoid');
const bcrypt = require('bcrypt-nodejs');
const Schema = mongoose.Schema;

const UserSchema = new Schema({
    username: {
        type: String,
        required: true,
        validate: {
            validator: async function (value) {
                if(!this.isModified('username')) return true;

                const user = await User.findOne({username: value});
                if(user) throw new Error('User already exists!');
                return true
            }
        },
        message: "Select another one"
    },
    password: {
        type: String,
        required: true
    },
    token: String,
    displayName: {
        type: String,
        required: true
    },
    phoneNumber: {
        type: Number,
        require: true
    }
});

UserSchema.pre('save', async function (next) {
    if(!this.isModified('password')) next();
    const SALT_WORK_FACTOR = 10;

    const salt = bcrypt.genSaltSync(SALT_WORK_FACTOR, (error, result) => {
        return result;
    });

    const hash = bcrypt.hashSync(this.password, salt);
    this.password = hash;
    console.log('Password is hashed and salted)')
    next();
});

UserSchema.methods.checkPassword = function(password) {
    return bcrypt.compareSync(password, this.password);
};

UserSchema.methods.generateToken = function(){
    this.token = nanoid();
};

UserSchema.set('toJSON', {
    transform: (doc, ret, options) => {
        delete ret.password;
        return ret;
    }
})

const User = mongoose.model('User', UserSchema);

module.exports = User;