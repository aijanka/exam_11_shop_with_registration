const express = require('express');
const Category = require('../models/Category');

const createRouter = () => {
    const router = express.Router();

    router.get('/', (req, res) => {
        Category.find()
            .then(categories => res.send(categories))
            .catch(error => res.status(404).send(error));
    });

    return router;
};

module.exports = createRouter;