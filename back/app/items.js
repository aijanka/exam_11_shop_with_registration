const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');

const Item = require('../models/Item');
const User = require('../models/User');
const Category = require('../models/Category');
const ObjectId = require('mongodb').ObjectId;
const auth = require('../middlewares/auth');

const config = require('../config');
const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname))
    }
});

const upload = multer({storage});

const createRouter = () => {
    const router = express.Router();

    router.get('/', (req, res) => {
        Item.find().populate('user','category')
            .then(items => res.send(items))
            .catch(error => res.status(404).send(error));
    });

    router.post('/', [auth, upload.single('image')], (req, res) => {
        console.log(req.file);
        const itemData = req.body;

        if(req.file) {
            itemData.image = req.file.filename;
        } else {
            itemData.image = null;
        }

        const item = new Item(itemData);
        item.save()
            .then(result => res.send(result))
            .catch(() => res.status(400).send());
    });

    router.get('/:id', (req, res) => {
        Item.findOne({_id: req.params.id})
            .then(result => {
                const item = result;
                User.findOne({_id: item.user}).then(user => {
                    item.user = user.username;
                });
                Category.findOne({_id: item.category}).then(category => {
                    item.category = category.name;
                });
                res.send(item);
            })
            .catch(() => res.sendStatus(500));
    });

    router.delete('/:id', auth, (req, res) => {
        const item = new Item();
        item.findOne({_id: req.params.id}).populate('user')
            .then(item => {
                if(!(item.user.equals(req.user.id))) res.status(403).send('Not your item!!!');
                else {
                    item.remove()
                    res.send('Item is deleted');
                }
            })
    });

    return router;
};

module.exports = createRouter;