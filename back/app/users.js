const express = require('express');
const User = require('../models/User');

const createRouter = () => {
    const router = express.Router();

    router.post('/', (req, res) => {
        const user = new User(req.body);
        user.save()
            .then(user => res.send(user))
            .catch(error => res.sendStatus(404));
    });

    router.post('/sessions', async (req, res) => {
        const user = await User.findOne({username: req.body.username});

        if(!user) res.status(401).send('User not found');

        const isMatch = await user.checkPassword(req.body.password);
        if(!isMatch) return res.status(401).send('Password is wrong!');

        user.generateToken();
        await user.save();
        res.send(200, user);
    });

    router.delete('/sessions', async(req, res) => {
        const token = req.get('Token');
        const success = {message: 'Logout success'};
        if(!token) return res.send(success);

        const user = await User.findOne({token});

        if(!user) return res.send(success);

        user.generateToken();
        await user.save();

        return res.send(success);
    });

    return router;
};

module.exports = createRouter;