const Users = require('../models/User');

const auth = (req, res, next) => {
    const token = req.get('Token');
    if (!token) {
        return res.status(401).send({error: 'Token not provided!'});
    }
    Users.findOne({token: token}).then(user => {
        if (!user) {
            return res.status(401).send('User not found!'); // Send error, no such user!
        }

        req.user = user;
        next();
    });
};

module.exports = auth;