import React from 'react';
import {Button, Col, Thumbnail} from "react-bootstrap";
import {LinkContainer} from "react-router-bootstrap";
import {Link, NavLink} from "react-router-dom";

const Item = props => {
    return (
        <Col xs={6} md={4}>
            <Thumbnail src={`http://localhost:8000/uploads/${props.image}`} alt={props.title}>
                <h3>{props.title}</h3>
                <p>{props.description}</p>
                <p>
                    <LinkContainer to={"/items/" + props.id} exact>
                        <Button>View more</Button>
                    </LinkContainer>
                </p>
            </Thumbnail>
        </Col>
    );
}

export default Item;