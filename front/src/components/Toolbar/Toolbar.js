import React from 'react';
import {Nav, Navbar, NavItem} from 'react-bootstrap';
import {LinkContainer} from "react-router-bootstrap";
import UserMenu from "../Menus/UserMenu";
import AnonymousMenu from "../Menus/AnonimousMenu";

const Toolbar = ({user, logout}) => (
    <Navbar>
        <Navbar.Header>
            <Navbar.Brand>
                <LinkContainer to="/" exact><a>My shop</a></LinkContainer>
            </Navbar.Brand>
            <Navbar.Toggle/>
        </Navbar.Header>
        <Navbar.Collapse>
            <Nav>
                <LinkContainer to="/items" exact>
                    <NavItem>All items</NavItem>
                </LinkContainer>
                {/*<LinkContainer to="/?category=food" exact>*/}
                    {/*<NavItem>Food</NavItem>*/}
                {/*</LinkContainer>*/}
                {/*<LinkContainer to="/?category=notebooks" exact>*/}
                    {/*<NavItem>Notebooks</NavItem>*/}
                {/*</LinkContainer>*/}
                {/*<LinkContainer to="/?category=homeAppl" exact>*/}
                    {/*<NavItem>Home Appliances</NavItem>*/}
                {/*</LinkContainer>*/}
                {/*<LinkContainer to="/?category=furniture" exact>*/}
                    {/*<NavItem>Furniture</NavItem>*/}
                {/*</LinkContainer>*/}

            </Nav>
            {user ? <UserMenu user={user} logout={logout}/> : <AnonymousMenu/>}
        </Navbar.Collapse>
    </Navbar>
)

export default Toolbar;
