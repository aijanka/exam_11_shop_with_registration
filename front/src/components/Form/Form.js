import React from 'react';
import {Col, ControlLabel, FormControl, FormGroup, HelpBlock} from "react-bootstrap";

const FormElement = props => (
    <FormGroup
        controlId={props.propertyName}
        validationState={props.error && 'error'}
    >
        <Col componentClass={ControlLabel} sm={2}>
            {props.title}
        </Col>
        <Col sm={10}>
            <FormControl
                type={props.type}
                required={props.required}
                placeholder={props.placeholder}
                name={props.propertyName}
                value={props.value}
                onChange={props.changeHandler}
                autoComplete={props.autoComplete}
            />
            {props.error &&
            <HelpBlock>{props.error}</HelpBlock>
            }

        </Col>
    </FormGroup>
);

export default FormElement;