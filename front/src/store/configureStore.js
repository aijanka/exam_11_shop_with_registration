import {createStore, applyMiddleware, compose, combineReducers} from 'redux';
import thunkMiddleware from 'redux-thunk';
import createHistory from 'history/createBrowserHistory';
import {routerMiddleware, routerReducer} from 'react-router-redux';



import itemsReducer from './reducers/items';
import usersReducer from './reducers/users';
import {saveState, loadState} from "./localStorage";


const rootReducer = combineReducers({
    items: itemsReducer,
    users: usersReducer,
    routing: routerReducer
});

export const history = createHistory();

const middleware = [
    thunkMiddleware,
    routerMiddleware(history)
]

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;



const persistedState = loadState();

const enhancers = composeEnhancers(applyMiddleware(...middleware));

const store = createStore(rootReducer, persistedState, enhancers);

store.subscribe(() => {
    saveState({
        users: store.getState().users
    })
});

export default store;