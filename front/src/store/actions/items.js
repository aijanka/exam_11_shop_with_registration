import axios from 'axios';
import {FETCH_ITEM_SUCCESS, FETCH_ITEMS_SUCCESS} from "./actionTypes";

export const fetchItemsSuccess = (items) => ({type: FETCH_ITEMS_SUCCESS, items});

export const fetchItems = () => {
    return dispatch => {
        axios.get('/items').then(response => {
            dispatch(fetchItemsSuccess(response.data));
        })
    }
};

export const fetchItemSuccess = (item) => ({type: FETCH_ITEM_SUCCESS, item});

export const fetchItem = (id) => {
    return dispatch => {
        console.log('in actions');
        axios.get('/items/' + id).then(response => {
            console.log('in axios');
            dispatch(fetchItemSuccess(response.data));
        })
    }
};