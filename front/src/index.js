import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from "react-redux";
import {ConnectedRouter} from "react-router-redux";
import axios from 'axios';

import './index.css';
import App from "./App";
import registerServiceWorker from './registerServiceWorker';
import store, {history} from './store/configureStore';
axios.defaults.baseURL = 'http://localhost:8000';

const app = (
    <Provider store={store}>
        <ConnectedRouter history={history}>
            <App/>
        </ConnectedRouter>
    </Provider>
);

ReactDOM.render(app, document.getElementById('root'));
registerServiceWorker();
