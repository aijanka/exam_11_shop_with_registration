import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {Button, Col, Form, FormGroup, PageHeader} from "react-bootstrap";
import {registerUser} from "../../store/actions/users";
import FormElement from "../../components/Form/Form";

class Register extends Component {
    state = {
        username: '',
        password: '',
        displayName: '',
        phoneNumber: ''
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    submitFormHandler = event => {
        event.preventDefault();
        this.props.registerUser(this.state)
    };

    hasErrorForField = (fieldName) => {
        // return this.props.error && this.props.error.errors[fieldName];
    }


    render() {
        return (
            <Fragment>
                <PageHeader>Register new user</PageHeader>
                <Form horizontal onSubmit={this.submitFormHandler}>

                    <FormElement
                        propertyName="username"
                        placeholder='Enter username'
                        title="Username"
                        type="text"
                        value={this.state.username}
                        changeHandler={this.inputChangeHandler}
                        // error={this.hasErrorForField('username') && this.props.error.errors.username.message}
                    />

                    <FormElement
                        propertyName="password"
                        placeholder='Enter password'
                        title='Password'
                        type="password"
                        value={this.state.password}
                        changeHandler={this.inputChangeHandler}
                        // error={this.hasErrorForField('password') && this.props.error.errors.password.message}
                    />

                    <FormElement
                        propertyName="displayName"
                        placeholder='Enter display name'
                        title='Display name'
                        type="text"
                        value={this.state.displayName}
                        changeHandler={this.inputChangeHandler}
                        // error={this.hasErrorForField('password') && this.props.error.errors.password.message}
                    />

                    <FormElement
                        propertyName="phoneNumber"
                        placeholder='Enter phone number'
                        title='Phone number'
                        type="number"
                        value={this.state.phoneNumber}
                        changeHandler={this.inputChangeHandler}
                        // error={this.hasErrorForField('password') && this.props.error.errors.password.message}
                    />

                    <FormGroup>
                        <Col smOffset={2} sm={10}>
                            <Button
                                bsStyle="primary"
                                type="submit"
                            >Register</Button>
                        </Col>
                    </FormGroup>
                </Form>
            </Fragment>
        );
    }

}

const mapStateToProps = state => ({
    error: state.users.registerError
});

const mapDispatchToProps = dispatch => ({
    registerUser: (userData) => dispatch(registerUser(userData))
});

export default connect(mapStateToProps, mapDispatchToProps)(Register);
