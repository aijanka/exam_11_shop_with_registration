import React, {Component} from 'react';
import {connect} from "react-redux";
import {Jumbotron} from "react-bootstrap";
import {fetchItem} from "../../store/actions/items";

class ItemDescription extends Component {
    componentDidMount() {
        console.log(this.props.match.params.id)
        this.props.fetchItem(this.props.match.params.id)
    }

    render() {
        // console.log(this.props.match.params.id);
        // if(!(this.props.items === {})) {
        //     console.log(this.props.items);
        //     const item = this.props.items.find(item => item._id === this.props.match.params.id);
        //     console.log(item);
        // }
        return (
            <Jumbotron>
                <h1>{this.props.items.title }</h1>
                <p>{this.props.items.description}</p>
                {/*<p>Seller name: {}</p>*/}
                {/*<p>Phone number: {}</p>*/}
            </Jumbotron>
        );
    }
}

const mapStateToProps = state => ({
    items: state.items.item
});

const mapDispatchToProps = dispatch => ({
    fetchItem: id => dispatch(fetchItem(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(ItemDescription);
