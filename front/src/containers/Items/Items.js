import React, {Component} from 'react';
import {connect} from "react-redux";
import {fetchItems} from "../../store/actions/items";
import {Grid, Row} from "react-bootstrap";
import Item from "../../components/Item/Item";

class Items extends Component {

    componentDidMount() {
        this.props.fetchItems();
        console.log(this.props);
    }

    render() {
        return (
            <Grid>
                <Row>
                    {this.props.items.map(item => {
                        console.log(this.props.items);
                        return (
                            <Item
                                key={item._id}
                                id={item._id}
                                title={item.title}
                                description={item.description}
                                image={item.image}
                            />
                        )
                    })}
                </Row>
            </Grid>
        );
    }
}

const mapStateToProps = state => ({
    items: state.items.items
});

const mapDispatchToProps = dispatch => ({
    fetchItems: () => dispatch(fetchItems())
});

export default connect(mapStateToProps, mapDispatchToProps)(Items);
