import React, {Component, Fragment} from 'react';
import {Route, Switch} from "react-router-dom";
import Layout from "./components/Layout/Layout";
import Items from "./containers/Items/Items";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import ItemDescription from "./containers/ItemDescription/ItemDescription";

class App extends Component {
    render() {
        return (
            <Fragment>
                <Layout>
                    <Switch>
                        <Route path='/' exact component={Items}/>
                        <Route path='/items' exact component={Items}/>
                        <Route path='/items/:id' exact component={ItemDescription}/>
                        {/*<Route path='/products/new' exact component={NewItem}/>*/}
                        <Route path='/register' exact component={Register}/>
                        <Route path='/login' exact component={Login}/>
                    </Switch>
                </Layout>
            </Fragment>
        );
    }
}

export default App;
